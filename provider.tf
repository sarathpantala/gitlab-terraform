provider "aws" {
  region  = var.aws_region
}

terraform {
  backend "s3" {
    bucket = "devops-gitlab-terraform-project"
    key    = "terraform-ec2-state"
    region = "us-east-2"
  }
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">= 4.0.0"
    }
  }
}
