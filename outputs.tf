output "aws-vpc-tf-id" {
  value = aws_vpc.vpc.id
}
output "aws-aws_instance-tf-id" {
  value = aws_instance.tf-test-server.id
}
output "aws-tf-ami-id" {
  value = aws_instance.tf-test-server.id
}
