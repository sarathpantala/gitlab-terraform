variable "aws_region" {
  description = "region"
  type        = string
}
variable "instance-type" {
  description = "instance type"
  type        = string
  default     = ""
}

variable "instance-key-name" {
  description = "ssh key"
  type        = string
  default     = ""
}

variable "iam-role-name" {
  description = "iam role name"
  type        = string
  default     = ""
}

variable "instance-associate-public-ip" {
  description = "ec2 instance pubic ip "
  type        = string
  default     = ""
}


variable "instance-tag-name" {
  description = "instance-tag-name"
  type        = string
  default     = ""
}

variable "vpc-cidr-block" {
  description = "CIDR  to the VPC"
  type        = string
  default     = ""
}

variable "subnet-cidr-block" {
  description = "CIDR to the subnet"
  type        = string
  default     = ""
}

variable "vpc-tag-name" {
  description = "VPC"
  type        = string
  default     = ""
}

variable "ig-tag-name" {
  description = "igw"
  type        = string
  default     = ""
}

variable "subnet-tag-name" {
  description = "subnet name"
  type        = string
  default     = ""
}

variable "sg-tag-name" {
  description = "sg name"
  type        = string
  default     = ""
}

variable "tf_ami_creation" {
  description = "creation of ami"
  type        = string
}
variable "s3_bucket_name" {
  description = "Name of s3 bucket"
  type        = string
}
variable "tf_s3_creation" {
  description = "creation of s3 bucket"
  type        = string
}