terraform {
  backend "s3" {
    bucket = "devops-gitlab-terraform-project"
    key    = "terraform-eks-state"
    region = "us-east-2"
  }
}
provider "aws" {
  region  = "us-east-2"
}
