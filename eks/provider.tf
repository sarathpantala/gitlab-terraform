provider "aws" {
  region  = var.region
}

terraform {
  backend "s3" {
    bucket = "devops-gitlab-terraform-project"
    key    = "terraform-eks-state"
    region = "us-east-2"
  }
  required_providers {
      aws = {
        source = "hashicorp/aws"
        version = ">= 4.0.0"
      }
  }

}
