terraform {
  backend "s3" {
    bucket = "devops-gitlab-terraform-project"
    key    = "terraform-ec2-state"
    region = "us-east-2"
  }
}
provider "aws" {
  region  = "us-east-2"
}
