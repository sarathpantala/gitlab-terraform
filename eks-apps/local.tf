locals{
    env = "dev"
    tf_test_dev_secret_arn = "arn:aws:secretsmanager:us-east-2:372182431220:secret:/dev/ops/vars-0WqAtv"
    tf_test_prod_secret_arn = "arn:aws:secretsmanager:us-east-2:372182431220:secret:/prod/ops/vars-cNe0yW"
    tf_test_secrets = jsondecode(data.aws_secretsmanager_secret_version.current.secret_string)

    tf_test_dev_env = {
        api_host = "prod-myhost:443"
        db_host  = "dev-dbhost:5432"
        CONFIG_PWD = local.tf_test_secrets["CONFIG_PWD"]
        DISCOVERY_PWD = local.tf_test_secrets["DISCOVERY_PWD"]
        CONFIG_CACHE_DB_PWD = local.tf_test_secrets["CONFIG_CACHE_DB_PWD"]
        DATABASE_URL = local.tf_test_secrets["DATABASE_URL"]

    }
    tf_test_prod_env = {
        api_host = "prod-myhost:443"
        db_host  = "prod-dbhost:5432"
        CONFIG_PWD = local.tf_test_secrets["CONFIG_PWD"]
        DISCOVERY_PWD = local.tf_test_secrets["DISCOVERY_PWD"]
        CONFIG_CACHE_DB_PWD = local.tf_test_secrets["CONFIG_CACHE_DB_PWD"]
        DATABASE_URL = local.tf_test_secrets["DATABASE_URL"]
    }
    tf_test_env_variables = local.env == "dev" ? local.tf_test_dev_env : local.tf_test_prod_env
    secret_arn = local.env == "dev" ? local.tf_test_dev_secret_arn : local.tf_test_prod_secret_arn
}
