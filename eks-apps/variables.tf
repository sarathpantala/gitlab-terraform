variable "secret_arn" {
  description = "Name of secret"
  type        = string
  default  = "refer locals"
}
variable "aws_region" {
  description = "region"
  type        = string
  default  = "us-east-2"
}