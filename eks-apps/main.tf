resource "kubernetes_namespace" "test-tf-server" {
  metadata {
    name = "terraform-tf-test-new"
  }
}

resource "kubernetes_config_map" "tf-test-server" {
  metadata {
    name = "test-tf-config"
    namespace = "terraform-tf-test-new"
  }

  data = local.tf_test_env_variables
}