provider "aws" {
  region  = var.aws_region
}
provider "kubernetes" {
  config_path    = "~/.kube/config"
}

terraform {
  backend "s3" {
    bucket = "devops-gitlab-terraform-project"
    key    = "terraform-eks-state-k8s"
    region = "us-east-2"
  }
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">= 4.0.0"
    }
  }
}