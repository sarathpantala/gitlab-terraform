resource "aws_s3_bucket" "bucket" {
  count  = var.tf_s3_creation > 0 ? 1 : 0
  bucket = var.s3_bucket_name

  tags = {
    Name        = "tf_state"
  }
}