data "aws_ami" "tf-test-server" {
  owners = ["372182431220"]

  filter {
    name   = "name"
    values = ["tf-test-server"]
  }
}
