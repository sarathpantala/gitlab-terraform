
data "aws_iam_policy_document" "iam-policy" {
  statement {
    sid = "1"
    actions = [
      "*"
    ]
    resources = [
      "*",
    ]
  }
}

resource "aws_iam_role" "tf-iam-role" {
  name               = "${var.instance-tag-name}-iam-role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": [
          "ec2.amazonaws.com"
        ]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}
resource "aws_iam_policy" "tf-iam-role" {
  name   = "${var.instance-tag-name}-iam-policy"
  policy = data.aws_iam_policy_document.iam-policy.json
}

resource "aws_iam_policy_attachment" "tf-iam-role" {
  name       = "${var.instance-tag-name}-policy-attachment"
  roles      = [aws_iam_role.tf-iam-role.name]
  policy_arn = aws_iam_policy.tf-iam-role.arn
}
resource "aws_iam_instance_profile" "tf-instance_profile" {
  name       = "${var.instance-tag-name}-intance-profile"
  role = aws_iam_role.tf-iam-role.name
}